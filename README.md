<!-- README.md -->

# Linking the GLFW modules manually

This project does just conatin a minimal working example to demonstrate
a linking problem.

Note: I am on a Debian bullseye, but do not have installed the
maintained packages/libraries `libglfw3`, `libglfw3-dev`, and/or `libglfw3-wayland`
from the distribution's  package manager.

## Requirements

To build the GLFW libraries from source the application `cmake` is used in `build_glfw.sh`.

Also, depending on the *display manager* of the operating system some
desktop development libraries are necessary, e.g. on Debian X-based systems `xorg-dev` 
is needed. (On Wayland `libwayland-dev`, `libxkbcommon-dev`, `wayland-protocols`,
and `extra-cmake-modules` have to be installed to build the GLFW libraries.) [[1][1]]


---


## Build application manually (TODO!)

```
$   ./build_glfw.sh

$   ./build_manually.sh               # TODO: Linking error!

$   ./main_YYYYMMDD_hhmmss_GLFW.exe
```


## Build application with CMake (Done.)
```
$   ./build_glfw.sh

$   ./build_with_cmake.sh

$   ./main_YYYYMMDD_hhmmss_GLFW_by_CMake.exe
```


--- 


## References

\[1] ... [Compiling GLFW](
    https://www.glfw.org/docs/latest/compile_guide.html
)

\[2] ... [Building applications using GLFW](
    https://www.glfw.org/docs/latest/build_guide.html
)

<!-- Reference marker -->
[1]: https://www.glfw.org/docs/latest/compile_guide.html

[2]: https://www.glfw.org/docs/latest/build_guide.html



<!-- eof -->

