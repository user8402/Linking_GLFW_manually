#! /usr/bin/env bash
set -euo pipefail

BLD_STAMP=$(date +%Y%m%d_%H%M%S)

PRJ_DIR=$(pwd)
SRC_DIR=${PRJ_DIR}/build/glfw/src
INC_DIR=${PRJ_DIR}/glfw/include

#   This command does not seem to work.
#   Maybe some library dependencies are missing?
#   TODO: Check the parameters.
g++ -std=c++2a \
	-I"${INC_DIR}" \
	-L"${SRC_DIR}" \
	-lglfw3 -lGL -lrt -lm -ldl --verbose \
	-o main_${BLD_STAMP}_GLFW.exe \
	main.cpp
#   ERROR MESSAGE:
#   "/usr/bin/ld: [...]
#    [...] undefined reference to `glfwInit'
#    collect2: error: ld returned 1 exit status"

# eof

