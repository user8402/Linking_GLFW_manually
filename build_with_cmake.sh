#! /usr/bin/env bash
set -euo pipefail

BLD_STAMP=$(date +%Y%m%d_%H%M%S)
BLD_CMAKE="./build/cmake"

mkdir -p ${BLD_CMAKE}

cmake -S . -B ${BLD_CMAKE}

make --directory=${BLD_CMAKE}

mv ${BLD_CMAKE}/Welcome2GLFW main_${BLD_STAMP}_GLFW_by_CMake.exe

# eof

