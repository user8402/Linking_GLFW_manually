#! /usr/bin/env bash
set -euo pipefail

SRC_DIR=./glfw
BLD_DIR=./build/glfw

if [ ! -d "${SRC_DIR}" ]
then
    if [ ! -d "./.git" ]
    then
        git init
    fi

    git submodule add https://github.com/glfw/glfw.git glfw
    git add glfw
    git commit -am "Initial inclusion of GLFW submodule"
fi

mkdir -p "${BLD_DIR}"

SESSION_TYPE=$(loginctl show-session $(awk '/tty/ {print $1}' <(loginctl)) -p Type | awk -F= '{print $2}')
# See: https://unix.stackexchange.com/questions/202891/

if [ "$SESSION_TYPE" = "wayland" ]
then
    cmake -S "${SRC_DIR}" -B "${BLD_DIR}" -D GLFW_USE_WAYLAND=1
else
    cmake -S "${SRC_DIR}" -B "${BLD_DIR}"
fi

make --directory="${BLD_DIR}"

# eof
