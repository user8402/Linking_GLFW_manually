cmake_minimum_required(VERSION 3.2)

project(Welcome2GLFW)

add_executable(${PROJECT_NAME} main.cpp)

# Meaning './glfw/':
add_subdirectory(glfw)

#   Meaning the './glfw/include/' source directory:
target_include_directories(${PROJECT_NAME}
    PUBLIC glfw/include
)

#   Adding a link library:
target_link_directories(${PROJECT_NAME}
    PRIVATE glfw/src
)
#   This time, meaning the project's build directory
#   './build/glfw/src/[libglfw3.a]'.

target_link_libraries(${PROJECT_NAME} glfw)

